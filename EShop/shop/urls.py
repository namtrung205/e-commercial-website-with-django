from django.urls import path, include
from . import views

app_name ='shop'
urlpatterns = [
    path('', views.shop, name="shop"),
    path('cart/', views.cart, name="cart"),
    path('checkout/', views.checkout, name="checkout"),
    path('<int:id>/', views.product_detail, name="product_detail"),
]
